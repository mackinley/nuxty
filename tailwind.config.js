const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  variants: {
    animation: ['responsive', 'hover', 'focus'],
  },
  theme: {
    container: {
      center: true,
    },
    extend: {
      typography: theme => ({
        DEFAULT: {
          css: {
            color: theme('colors.gray.800'),
            a: {
              color: theme('colors.blue.600'),
              "text-decoration": 'underline',
              '&:hover': {
                color: theme('colors.white'),
                background: theme('colors.blue.900'),
              },
            },
            // ...
          },
        },
      }),
    }
  },
  plugins: [
    require('@tailwindcss/typography'),
    // ...
  ],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
  },
}
