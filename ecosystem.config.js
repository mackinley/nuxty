module.exports = {
  apps : [{
    name: 'nuxty',
    script: 'yarn',
    args: 'start',
    env: {
      PORT: 3000
    },
    instances: 1,
    autorestart: true,
    watch: '.nuxt/dist/server/server.manifest.json',
    max_memory_restart: '1G'
  }]
};
