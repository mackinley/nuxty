const {
  IntrospectionFragmentMatcher,
  InMemoryCache,
} = require('apollo-cache-inmemory')
const introspectionQueryResultData = require('~/fragmentTypes.json')

export default function ({ $config }) {
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    // process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0",
    introspectionQueryResultData,
  })

  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

  // console.log('apollo')
  // console.log(`${$config.craftUrl}/api`)

  return {
    httpEndpoint: `${$config.craftUrl}/api`,
    cache: new InMemoryCache({ fragmentMatcher }),
  }
}
