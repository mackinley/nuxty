const fs = require('fs')
const dotenv = require('dotenv')
const fetch = require('node-fetch')
dotenv.config()

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const isDev = process.env.NODE_ENV === 'development'

// fetch(`${process.env.CRAFT_URL}/api`, {
fetch(`${process.env.CRAFT_URL}/api`, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    variables: {},
    query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `
  })
})
  .then(result => result.json())
  .then(({ data }) => {
    const filteredData = data.__schema.types.filter(type => type.possibleTypes !== null)

    data.__schema.types = filteredData

    try {
      fs.writeFileSync('./fragmentTypes.json', JSON.stringify(data, null, isDev ? 1 : 0))
      console.log("fragmentTypes.json created")
    } catch (err) {
      console.error(err)
    }
  })
