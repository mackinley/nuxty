import { cloneDeep, isFunction } from 'lodash'

export default ({
  query,
  transform,
  variables = () => {
    return {}
  },
}) => {
  return {
    asyncData(context) {
      const { app, error } = context
      return app.apolloProvider.defaultClient
        .query({ query, variables: variables(context) })
        .then(({ data }) => {
          return isFunction(transform) ? transform(cloneDeep(data)) : data
        })
        .catch(({ message, status }) => {
          error({
            statusCode: status,
            message,
          })
        })
    },
  }
}
