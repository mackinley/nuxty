module.exports = {
  root: true,
  env: {
    node: true,
    browser: true
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  rules: {
    'nuxt/no-cjs-in-config': 'off',
    'vue/no-v-html': 'off',
    'no-console': 'off',
    'space-before-function-paren': 'off',
    'comma-dangle': 'off',
    semi: 'off',
    quotes: 'off'
  },
  globals: {
    $nuxt: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
