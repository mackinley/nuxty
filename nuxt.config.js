import tailwind from './tailwind.config.js'

const isDev = process.env.NODE_ENV === 'development'
// const themeColor = tailwind.theme.colors.grey

module.exports = {
  mode: 'universal',
  head: {
    title: 'nuxty',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        name: 'msapplication-TileColor',
        content: '#ccf6e8',
      },
      {
        name: 'application-name',
        content: 'Nuxty',
      },
      {
        name: 'msapplication-config',
        content: '/favicon/browserconfig.xml',
      },
      {
        name: 'theme-color',
        // content: themeColor,
      },
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/favicon/apple-touch-icon.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png',
      },
      {
        rel: 'manifest',
        href: '/favicon/site.webmanifest',
      },
      {
        rel: 'mask-icon',
        href: '/favicon/safari-pinned-tab.svg',
        // color: themeColor,
      },
    ],
  },
  loading: {
    // color: tailwind.theme.colors.navy[200],
    continuous: true,
    height: '4px',
  },
  css: [
    // 'plyr/dist/plyr.css'
  ],
  plugins: [
    // {
    //   src: '~/plugins/rect.js',
    //   ssr: false
    // },
    // {
    //   src: '~/plugins/scroll.js',
    //   ssr: false
    // },
    // '~/plugins/v-scroll',
    // {
    //   src: '~/plugins/webfontloader.js',
    //   ssr: false
    // }
  ],
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss',
    [
      'nuxt-mq',
      {
        defaultBreakpoint: 'default',
        breakpoints: {
          default: 640,
          sm: 768,
          md: 1024,
          lg: 1280,
          xl: 1440,
          '2xl': Infinity,
        },
      },
    ],
  ],
  modules: [
    '@nuxtjs/apollo',
    '@nuxt/http',
    '@nuxtjs/proxy',
    'nuxt-lazy-load',
    [
      '@nuxtjs/redirect-module',
      {
        rules: [
          {
            from: '^/admin',
            to: `${process.env.CRAFT_URL}/admin`,
          },
        ],
      },
    ],
  ],
  apollo: {
    clientConfigs: {
      default: '~/apollo.config.js',
    },
  },
  proxy: ['*.xml', 'humans.txt', 'robots.txt'].map(
    (uri) => `${process.env.CRAFT_URL}/${uri}`
  ),
  publicRuntimeConfig: {
    craftUrl: process.env.CRAFT_URL,
  },
  modern: isDev ? false : 'server',
  build: {
    babel: {
      presets({ isServer }) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'),
            {
              buildTarget: isServer ? 'server' : 'client',
              corejs: { version: 3 },
            },
          ],
        ]
      },
    },
    extractCSS: true,
    extend({ module }, ctx) {},
  },
}
